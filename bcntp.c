//#!/usr/bin/tcc -run
// Reads whole input and outputs the same but prepends it with an 8 byte binary size prefix.
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
int main (){
 #define blocksize 4

 #define pages(a) (4096*a)
 #define blocks(f) (pages(blocksize)*f)
 unsigned long
  blks= 1,
  count= 0, // Total count of bytes
  r= 0;
 char
  * b= malloc (4096);
 while (r= read (0, b+count, 4096),r>0) count+= r, b= realloc (b, count+4096);
 write (1, &count, 8);
 write (1, b, count);
 return 0;
}
