Just prepends data from stdin with an 8 byte count of all the bytes it recieves and writes to stdout.
Not particularly fast or memory efficient but easy to wrap your head around and useful for computing generic data that is not time critical.
